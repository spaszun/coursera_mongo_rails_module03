class Event
  include Mongoid::Document

  field :o, as: :order, type: Integer
  field :n, as: :name, type: String
  field :d, as: :distance, type: Float
  field :u, as: :units, type: String

  validates :order, :presence => true
  validates :name, :presence => true

  def meters
    case self.units
      when 'meters' then self.distance
      when 'kilometers' then 1000 * self.distance
      when 'yards' then 0.9144 * self.distance
      when 'miles' then 1609.34 * self.distance
    end
  end

  def miles
    case self.units
      when 'meters' then 0.000621371 * self.distance
      when 'kilometers' then 0.621371 * self.distance
      when 'yards' then 0.000568182 * self.distance
      when 'miles' then self.distance
    end
  end

  embedded_in :parent, polymorphic: true, touch: true

end
