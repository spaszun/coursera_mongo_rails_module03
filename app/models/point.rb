class Point

  attr_accessor :longitude, :latitude

  def initialize(lng, lat)
    @longitude = lng
    @latitude = lat
  end

  def mongoize
    return {:type=>'Point', :coordinates=>[@longitude,@latitude]}
  end

  def self.mongoize(object)
    case object
      when Point then object.mongoize
      when Hash then
        if object[:type] #in GeoJSON Point format
          Point.new(object[:coordinates][0], object[:coordinates][1]).mongoize
        else       #in legacy format
          Point.new(object[:lng], object[:lat]).mongoize
        end
      else object
    end
  end

  def self.demongoize(object)
    case object
      when Point then object
      when Hash then
        if object[:type] #in GeoJSON Point format
          if object[:coordinates]
            Point.new(object[:coordinates][0], object[:coordinates][1])
          else
            Point.new(nil, nil)
          end
        else       #in legacy format
          Point.new(object[:lng], object[:lat])
        end
      else object
    end
  end

  def self.evolve(object)
    case object
      when Point then object.mongoize
      else object
    end
  end
end